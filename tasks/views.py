from django.shortcuts import render, redirect
from tasks.models import Task
from tasks.forms import CreateTaskForm
from django.contrib.auth.decorators import login_required

# Create your views here.


def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")

    else:
        form = CreateTaskForm()
    context = {"form": form}
    return render(request, "create_task.html", context)


@login_required
def show_tasks(request):
    user = request.user
    tasks = Task.objects.filter(assignee=user)

    context = {
        "tasks": tasks,
    }
    return render(request, "show_tasks.html", context)
