from django.shortcuts import render, redirect, get_object_or_404
from projects.models import Project
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm


@login_required
def list_projects(request):
    user = request.user
    projects = Project.objects.filter(owner=user)

    context = {"projects": projects}
    return render(request, "projects.html", context)


# Create your views here.


@login_required
def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project=project)
    context = {"project": project, "tasks": tasks}

    return render(request, "show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()

            return redirect("list_projects")
    else:
        form = CreateProjectForm()

    context = {"form": form}
    return render(request, "create.html", context)
